package org.terra.demo.textris.shape;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public interface Piece {

	static int SIZE = 3;
	char STAR = '*';
	char EMPTY = ' ';

	Piece copy();

	char[][] getGrid();

	void setGrid(char grid[][]);
	
	String getFileName();

	default void turnClockwise() {
		char[][] newArray = new char[getGrid().length][getGrid().length];
		for (int i = 0; i < newArray.length; i++) {
			for (int j = 0; j < newArray[0].length; j++) {
				newArray[i][j] = getGrid()[newArray.length - j - 1][i];
			}
		}
		setGrid(newArray);
	}

	default void turnAntiClockwise() {
		char[][] newArray = new char[getGrid().length][getGrid().length];
		for (int i = 0; i < newArray.length; i++) {
			for (int j = 0; j < newArray[0].length; j++) {
				newArray[i][j] = getGrid()[j][newArray.length - i - 1];
			}
		}
		setGrid(newArray);
	}
	
	default void load() {
		
		File file = new File(LShapePiece.class.getClassLoader().getResource(getFileName()).getFile());

		

		try (Scanner in = new Scanner(file)) {
			int n = Integer.parseInt(in.nextLine());
			char[][] pieceGrid = new char[n][n];	
			for (int i = 0; i < n; i++) {
				try (Scanner line = new Scanner(in.nextLine())) {
					for (int j = 0; j < n; j++) {
						int s = line.nextInt();
						pieceGrid[i][j] = s == 1 ? STAR : EMPTY;
					}
				}
			}
			setGrid(pieceGrid);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
	}

}
