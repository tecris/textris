package org.terra.demo.textris.shape;

/**
 * Representation for mirror L shaped piece
 * 
 */
public class MirrorLShapePiece implements Piece {

	private static final String FILE_NAME = "mirror_l_shape.txt";

	private char pieceGrid[][];

	public MirrorLShapePiece() {
		load();
	}

	@Override
	public char[][] getGrid() {
		return pieceGrid;
	}

	@Override
	public void setGrid(char[][] grid) {
		this.pieceGrid = grid;
	}

	@Override
	public Piece copy() {
		MirrorLShapePiece piece = new MirrorLShapePiece();
		piece.pieceGrid = this.pieceGrid;
		return piece;
	}

	@Override
	public String getFileName() {
		return MirrorLShapePiece.FILE_NAME;
	}
}
