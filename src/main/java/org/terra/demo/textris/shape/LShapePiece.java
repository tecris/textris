package org.terra.demo.textris.shape;

/**
 * Representation for L shaped piece
 * 
 */
public class LShapePiece implements Piece {

	private char pieceGrid[][];

	private static final String FILE_NAME = "l_shape.txt";

	public LShapePiece() {
		load();
	}
	
	public String getFileName() {
		return LShapePiece.FILE_NAME;
	}

	@Override
	public char[][] getGrid() {
		return pieceGrid;
	}

	@Override
	public void setGrid(char[][] grid) {
		this.pieceGrid = grid;
	}

	@Override
	public Piece copy() {
		LShapePiece piece = new LShapePiece();
		piece.pieceGrid = this.pieceGrid;
		return piece;
	}

}
