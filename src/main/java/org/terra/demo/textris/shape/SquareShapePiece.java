package org.terra.demo.textris.shape;

/**
 * Representation for square shaped piece
 * 
 */
public class SquareShapePiece implements Piece {

	private static final String FILE_NAME = "step_shape.txt";

	private char pieceGrid[][];

	public SquareShapePiece() {
		load();
	}

	@Override
	public char[][] getGrid() {
		return pieceGrid;
	}

	@Override
	public void setGrid(char[][] grid) {
		this.pieceGrid = grid;
	}

	@Override
	public Piece copy() {
		SquareShapePiece piece = new SquareShapePiece();
		piece.pieceGrid = this.pieceGrid;
		return piece;
	}

	@Override
	public String getFileName() {
		return SquareShapePiece.FILE_NAME;
	}
}
