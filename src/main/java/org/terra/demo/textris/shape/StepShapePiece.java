package org.terra.demo.textris.shape;

/**
 * Representation for steps shaped piece
 * 
 */
public class StepShapePiece implements Piece {

	private static final String FILE_NAME = "step_shape.txt";

	private char pieceGrid[][];

	public StepShapePiece() {
		load();
	}

	@Override
	public char[][] getGrid() {
		return pieceGrid;
	}

	@Override
	public void setGrid(char[][] grid) {
		this.pieceGrid = grid;
	}

	@Override
	public Piece copy() {
		StepShapePiece piece = new StepShapePiece();
		piece.pieceGrid = this.pieceGrid;
		return piece;
	}

	@Override
	public String getFileName() {
		return StepShapePiece.FILE_NAME;
	}
}
