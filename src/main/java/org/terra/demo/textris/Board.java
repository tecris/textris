package org.terra.demo.textris;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.terra.demo.textris.shape.LShapePiece;
import org.terra.demo.textris.shape.MirrorLShapePiece;
import org.terra.demo.textris.shape.Piece;
import org.terra.demo.textris.shape.SquareShapePiece;
import org.terra.demo.textris.shape.StepShapePiece;

public class Board {

	public static final int GRID_SIZE = 20;
	private char grid[][] = new char[GRID_SIZE][GRID_SIZE];

	private static final List<Piece> PIECE_TYPES = new ArrayList<>();

	private Piece piece;
	private int xActivePiece;
	private int yActivePiece;

	public Board() {

		grid = this.buildBlankGrid();

		PIECE_TYPES.addAll(this.initPieces());

		resetActivePiece();
	}

	private char[][] buildBlankGrid() {

		char blankGrid[][] = new char[GRID_SIZE][GRID_SIZE];
		for (int y = 0; y < GRID_SIZE; y++) {
			for (int x = 0; x < GRID_SIZE; x++) {
				blankGrid[y][x] = isBoundary(x, y) ? Piece.STAR : Piece.EMPTY;
			}
		}
		return blankGrid;
	}

	private List<Piece> initPieces() {
		List<Piece> pieceList = new ArrayList<>();

		pieceList.add(new LShapePiece());
		pieceList.add(new MirrorLShapePiece());
		pieceList.add(new SquareShapePiece());
		pieceList.add(new StepShapePiece());
		return pieceList;
	}


	public void run() {

		draw();
		while (true) {
			Scanner reader = new Scanner(System.in);
			System.out.println("Enter a command: ");
			Command command = null;
			try {
				command = Command.getCommand(reader.next().charAt(0));
				System.out.print("\u001b[2J");
				System.out.flush();
				executeCommand(command);
				checkActivePieceInFinalPosition();
				draw();
			} catch (IllegalArgumentException ex) {
				System.out.println("Invalid command: " + ex.getMessage());
			}
		}
	}

	private void executeCommand(Command command) {

		switch (command) {
		case MOVE_LEFT:
			if (this.canMoveLeft(piece, xActivePiece, yActivePiece)) {
				--xActivePiece;
				++yActivePiece;
			}
			break;
		case MOVE_RIGHT:
			if (this.canMoveRight(piece, xActivePiece, yActivePiece)) {
				++xActivePiece;
				++yActivePiece;
			}
			break;
		case ROTATE_CLOCKWISE:
			if (this.canTurnClockwise(piece, xActivePiece, yActivePiece)) {
				piece.turnClockwise();
				++yActivePiece;
			}
			break;
		case ROTATE_COUNTER_CLOCKWISE:
			if (this.canTurnAntiClockwise(piece, xActivePiece, yActivePiece)) {
				piece.turnAntiClockwise();
				++yActivePiece;
			}
			break;
		}
	}

	private boolean checkActivePieceInFinalPosition() {

		boolean finalPosition = false;
		char sketchGrid[][] = addPieceToEmptyBoard(piece, xActivePiece, yActivePiece);
		if (!canMove(piece, xActivePiece, yActivePiece)) {
			for (int y = 0; y < grid.length - 1; y++) {
				for (int x = 1; x < grid[0].length - 1; x++) {
					if (sketchGrid[y][x] == Piece.STAR) {
						grid[y][x] = Piece.STAR;
					}
				}
			}

			resetActivePiece();
			finalPosition = true;
		}
		return finalPosition;
	}

	private boolean canMove(Piece movePiece, int pieceX, int pieceY) {
		return canMoveLeft(movePiece, xActivePiece, yActivePiece)
				|| canMoveRight(movePiece, xActivePiece, yActivePiece)
				|| canTurnClockwise(movePiece, xActivePiece, yActivePiece)
				|| canTurnAntiClockwise(movePiece, xActivePiece, yActivePiece);
	}

	private boolean canMoveLeft(Piece piece, int pieceX, int pieceY) {
		return isValidMove(piece, --pieceX, ++pieceY);
	}

	private boolean canMoveRight(Piece piece, int pieceX, int pieceY) {
		return isValidMove(piece, ++pieceX, ++pieceY);
	}

	private boolean canTurnClockwise(Piece piece, int pieceX, int pieceY) {
		piece = piece.copy();
		piece.turnClockwise();
		return isValidMove(piece, pieceX, ++pieceY);
	}

	private boolean canTurnAntiClockwise(Piece piece, int pieceX, int pieceY) {
		piece = piece.copy();
		piece.turnAntiClockwise();
		return isValidMove(piece, pieceX, ++pieceY);
	}

	private boolean isValidMove(Piece piece, int pieceX, int pieceY) {

		boolean fits = true;
		char sketchGrid[][] = addPieceToEmptyBoard(piece, pieceX, pieceY);

		if (!isWithinBoundaries(sketchGrid)) {
			return false;
		}

		outer: for (int y = 0; y < grid.length - 1; y++) {
			for (int x = 1; x < grid[0].length - 1; x++) {
				if (sketchGrid[y][x] == Piece.STAR) {
					if (grid[y][x] == Piece.STAR) {
						fits = false;
						break outer;
					}
				}
			}
		}
		return fits;
	}

	private void resetActivePiece() {
		piece = PIECE_TYPES.get(new Random().nextInt(PIECE_TYPES.size()));
		xActivePiece = getRandomCoordinate();
		yActivePiece = 0;
	}

	public void draw() {

		char pieceGrid[][] = addPieceToEmptyBoard(piece, xActivePiece, yActivePiece);

		for (int y = 0; y < pieceGrid.length; y++) {
			for (int x = 0; x < pieceGrid[0].length; x++) {
				System.out.format("%2s", grid[y][x] == Piece.STAR || pieceGrid[y][x] == Piece.STAR ? Piece.STAR : Piece.EMPTY);
			}
			System.out.println("");
		}
	}

	private char[][] addPieceToEmptyBoard(Piece piece, int pieceX, int pieceY) {
		char pieceGrid[][] = piece.getGrid();
		char virtualGrid[][] = new char[GRID_SIZE][GRID_SIZE];
		for (int y = 0; y < virtualGrid.length; y++) {
			for (int x = 0; x < virtualGrid[0].length; x++) {
				virtualGrid[y][x] = drawPiece(y, x, pieceX, pieceY, pieceGrid.length-1) ? pieceGrid[y - pieceY][x - pieceX] : Piece.EMPTY;
			}
		}
		return virtualGrid;
	}

	private boolean isWithinBoundaries(char[][] gridToCheck) {
		boolean isWithinBoundaries = true;
		outer: for (int y = 0; y < gridToCheck.length; y++) {
			for (int x = 0; x < gridToCheck[0].length; x++) {
				if (isBoundary(x, y) && gridToCheck[y][x] == Piece.STAR) {
					isWithinBoundaries = false;
					break outer;
				}
			}
		}
		return isWithinBoundaries;

	}

	private boolean drawPiece(int y, int x, int pieceX, int pieceY, int gridSize) {
		return pieceY <= y && y <= pieceY + gridSize && pieceX <= x && x <= pieceX + gridSize;

	}

	private static boolean isBoundary(int x, int y) {
		return x == 0 || x == GRID_SIZE - 1 || y == GRID_SIZE - 1;
	}

	private int getRandomCoordinate() {
		return 1 + new Random().nextInt(GRID_SIZE - piece.getGrid()[0].length - 1);
	}

}
