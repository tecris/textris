package org.terra.demo.textris;

public enum Command {
	
	MOVE_LEFT('a'), MOVE_RIGHT('d'), ROTATE_COUNTER_CLOCKWISE('w'), ROTATE_CLOCKWISE('s');
	
	private char command;
	
	Command(char command){
		this.command = command;
	}

	/**
	 * Returns corresponding Command for given char
	 * Throws IllegalArgumentException if c is not one of 'a', 'd', 'w', 's'
	 * 
	 * @param c char to map to command
	 * @return Command for given char
	 */
	public static Command getCommand(char c) {
		for(Command cmd : Command.values()) {
			if(c == cmd.command) {
				return cmd;
			}
		}
		throw new IllegalArgumentException("Invalid command character '"+c+"', expected one of 'a', 'd', 'w', 's'");
	}
}
